//dashboard app
/*
1. Weather
2. Notes
3. Calculator
4. Move elements around
5. Calendar add to schedule
*/

import React, {Component} from 'react';
import ReactDOM from 'react-dom'; //render react elements on the browser

const api_key = '8da924c12b2a065936fe8cc921f18e95';

class App extends Component{
  //inherts useful methods in COmpoenent like render
   constructor(props){
     //props is a keyword that checks for data construcotrs
     super(props);
     this.state = {//city property is state object
       city: '', description: ''
     }
       this.handleChange = this.handleChange.bind(this);
       this.handleSubmit = this.handleSubmit.bind(this);
   }
    handleChange(event){
       this.setState({city: event.target.value});
    }
    handleSubmit(event){
        this.grabWeather(this.state.city);
        event.preventDefault();
    }

   grabWeather(city){
       function handleErrors(response) {
           if (!response.ok) {
               alert("error");
           }
           return response;
       }
       //fetch data from api
       fetch(`http://api.openweathermap.org/data/2.5/weather?APPID=${api_key}&q=${city}`)
           .then(handleErrors)
           .then(response => response.json()) //handles promise
           .then(json => {
             this.setState({
                 description:json.weather[0].description,
                 main: json.weather[0].main
             }).catch(error => alert(error) );
              // console.log(json.weather);
           });
   }

    render(){
    //jsx add xml syntax to javaScript
      return(
          <div>
              <form onSubmit={this.handleSubmit}>
                  <label>
                      Enter City:
                      <input type="text" value={this.state.city} onChange={this.handleChange}/>
                  </label>
                  <input type="submit" value="Submit"/>
              </form>
            <h1>Weather Report for {this.state.city}!</h1>
            <h2>{this.state.description}, {this.state.main}!</h2>
          </div>
    )
  }
}
ReactDOM.render(<App/>, document.getElementById('root'));